import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

// Класс построения HTML документа
public class HTMLBuilder {

    public void build(File htmlFile, XMLParser xmlFile) {
        int size = xmlFile.RectList.size();
        TableMatrix m = new TableMatrix(2 * size + 1);
        MasTool tool = new MasTool(2 * size);

        // Добавление значений координат прямоугольников в матрицу по осям
        // X и Y для расстановки крайних точек каждого (правых нижних)
        m.addXAxisCoords(tool.getSortedXCoords(xmlFile));
        m.addYAxisCoords(tool.getSortedYCoords(xmlFile));

        // Поиск последнего сегмента (правого нижнего) от каждого прямоугольника
        for (int i = 1; i < m.size; i++) {
            for (int j = 1; j < m.size; j++) {
                for (Rectangle object: xmlFile.RectList) {
                    if ( m.matrix[0][j] == object.rxcoord & m.matrix[i][0] == object.rycoord ) {
                        m.matrix[i][j] = xmlFile.RectList.indexOf(object);
                        object.lastSegmentX = i;
                        object.lastSegmentY = j;
                    }
                }
            }
        }

        // Приведение матрицы к "сегментному"
        // виду вместо координатного
        m.toWidthX();
        m.toWidthY();

        // Для каждого прямоугольника ищем все сегменты
        // для закраски в матрице (расстановка индексов
        // соответсвующих прямоугольников)
        for (Rectangle object: xmlFile.RectList)
            m.paintSegments(object);

        m.print();

        // Создание текста HTML документа
        String htmlContent;
        StringBuffer stringBuffer = new StringBuffer(HTMLStrings.BEGIN);
        for (int i = 1; i < m.size; i++) {
            stringBuffer.append(HTMLStrings.TRBEGIN)
                        .append(m.matrix[i][0])
                        .append(HTMLStrings.TREND);
            for (int j = 1; j < m.size; j++) {
                stringBuffer.append(HTMLStrings.TDBEGIN)
                            .append(m.matrix[0][j])
                            .append(HTMLStrings.TDMIDDLE)
                            .append(m.matrix[i][j] > -1 ? "444444" : "FFFFFF")
                            .append(HTMLStrings.TDEND);
            }
        }
        stringBuffer.append(HTMLStrings.END);
        htmlContent = stringBuffer.toString();
        byte[] buffer = htmlContent.getBytes();
    try {
        FileOutputStream fos = new FileOutputStream(htmlFile);
        fos.write(buffer, 0, buffer.length);
    } catch (IOException e) {
        e.printStackTrace();
    }
}
}
