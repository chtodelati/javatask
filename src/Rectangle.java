// Класс прямоугольника
public class Rectangle {

    public int lxcoord;
    public int lycoord;
    public int rxcoord;
    public int rycoord;
    public int width;
    public int height;
    public int lastSegmentX;
    public int lastSegmentY;

    Rectangle(int lx, int ly, int rx, int ry) {
        lxcoord = lx;
        lycoord = ly;
        rxcoord = rx;
        rycoord = ry;
        this.calculateSizes();
    }

    public void calculateSizes() {
        width = rxcoord - lxcoord;
        height = rycoord - lycoord;
    }
}
