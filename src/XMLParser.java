import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.util.ArrayList;

// Класс-парсер файла XML с последующим
// сохранением обьектов-прямоугольников
public class XMLParser {

    public ArrayList<Rectangle> RectList;

    public void parse(File xmlFile) {
        RectList = new ArrayList<>();
        try {
            DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
            DocumentBuilder b = f.newDocumentBuilder();
            Document doc = b.parse(xmlFile);
            NodeList list = doc.getElementsByTagName("rect");

            for (int temp = 0; temp < list.getLength(); temp++) {
                Node node = list.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    Rectangle rect = new Rectangle( getValue(element, "leftUpSide", "x"),
                                                    getValue(element, "leftUpSide", "y"),
                                                    getValue(element, "rightDownSide", "x"),
                                                    getValue(element, "rightDownSide", "y"));
                    RectList.add(rect);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getValue(Element e, String side, String axis) {
        String tempVal = e.getElementsByTagName(side).item(0).getAttributes().getNamedItem(axis).getNodeValue();
        return Integer.parseInt(tempVal);
    }
}
