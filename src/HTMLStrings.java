// Класс-хранилище константных значений
// строк для построения HTML файла
public class HTMLStrings {
    public static String BEGIN      =   "<!DOCTYPE HTML>\n" +
                                        "<html>\n" +
                                        "<head>\n" +
                                        "<meta charset=\"utf-8\">\n" +
                                        "<title>Rectangles</title>\n" +
                                        "</head>\n" +
                                        "<body style=\"margin: 0px;padding: 0px;\">\n" +
                                        "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n";
    public static String TRBEGIN    =   "<tr height=\"";
    public static String TREND      =   "px;\">\n";
    public static String TDBEGIN    =   "<td width=\"";
    public static String TDMIDDLE   =   "\" bgcolor=\"#";
    public static String TDEND      =   "\"></td>\n";
    public static String END        =   "</table>\n" +
                                        "</body>\n" +
                                        "</html>";
}
