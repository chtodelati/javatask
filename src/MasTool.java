import java.util.Arrays;

// Инструмент для извлечения координат по различным осям
// с их последующей сортировкой
public class MasTool {

    private int tempCoords[];

    public MasTool(int s) {
        tempCoords = new int[s];
    }

    public int [] getSortedXCoords(XMLParser xmlFile) {
        for (int i = 0; i < tempCoords.length; i += 2) {
            tempCoords[i] = xmlFile.RectList.get(i / 2).lxcoord;
            tempCoords[i + 1] = xmlFile.RectList.get(i / 2).rxcoord;
        }
        Arrays.sort(tempCoords);
        return tempCoords;
    }

    public int [] getSortedYCoords(XMLParser xmlFile) {
        for (int i = 0; i < tempCoords.length; i += 2) {
            tempCoords[i] = xmlFile.RectList.get(i / 2).lycoord;
            tempCoords[i + 1] = xmlFile.RectList.get(i / 2).rycoord;
        }
        Arrays.sort(tempCoords);
        return tempCoords;
    }
}
