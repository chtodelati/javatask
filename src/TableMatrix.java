public class TableMatrix {

    public int matrix[][];
    public int size;

    public TableMatrix(int s) {
        size = s;
        matrix = new int[size][size];
        this.init();
    }

    private void init() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrix[i][j] = -1;
            }
        }
    }

    // Добавление всех значений координат прямоугольников по оси X
    public void addXAxisCoords(int [] tempCoords) {
        for (int i = 0, j = 1; j < size; j++)
            matrix[i][j] = tempCoords[j - 1];
    }

    // Добавление всех значений координат прямоугольников по оси Y
    public void addYAxisCoords(int [] tempCoords) {
        for (int i = 1, j = 0; i < size; i++)
            matrix[i][j] = tempCoords[i - 1];
    }

    // Приведение матрицы к "сегментному" виду по оси X
    public void toWidthX() {
        for (int i = size - 1, j = 0; i > 1; i--)
            matrix[i][j] = matrix[i][j] - matrix[i - 1][j];
    }

    // Приведение матрицы к "сегментному" виду по оси Y
    public void toWidthY() {
        for (int j = size - 1, i = 0; j > 1; j--)
            matrix[i][j] = matrix[i][j] - matrix[i][j - 1];
    }

    // Расстановка индексов прямоугольников в матрице
    public void paintSegments(Rectangle rect) {
        int pixelsX = rect.width;
        int pixelsY = rect.height;
        int curIndex = matrix[rect.lastSegmentX][rect.lastSegmentY];
        int lastI = 0, lastJ = 0;
        int i, j;

        for (i = rect.lastSegmentX; i > 0; i--) {
            if (matrix[i][0] < pixelsY) {
                pixelsY -= matrix[i][0];
            } else {
                lastI = i;
                break;
            }
        }
        for (j = rect.lastSegmentY; j > 0; j--) {
            if (matrix[0][j] < pixelsX) {
                pixelsX -= matrix[0][j];
            } else {
                lastJ = j;
                break;
            }
        }
        for (i = rect.lastSegmentX; i >= lastI ; i--) {
            for (j = rect.lastSegmentY; j >= lastJ ; j--) {
                matrix[i][j] = curIndex;
            }
        }
    }

    // Вывод матрицы
    public void print() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }

}
