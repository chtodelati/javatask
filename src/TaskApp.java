import java.io.File;
import java.io.IOException;

// Главный класс приложения
public class TaskApp {

    private void run(String arg) {
        File xmlFile = new File("C://" + arg + ".xml");
        File htmlFile = new File("C://output.html");
        XMLParser parser = new XMLParser();
        HTMLBuilder builder = new HTMLBuilder();
        System.out.println("Argument: " + arg);
        try {
            boolean isCreated = htmlFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (xmlFile.exists()) {
            parser.parse(xmlFile);
            builder.build(htmlFile, parser);
        } else {
            System.out.println("There is no XML file!");
        }
    }

    public static void main(String[] args) {
        if(args.length != 0) {
            TaskApp application = new TaskApp();
            application.run(args[0]);
        }
    }
}